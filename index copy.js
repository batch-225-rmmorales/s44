const txtTitle = document.querySelector("#txt-title");
const txtBody = document.querySelector("#txt-body");
const formAddPost = document.querySelector("#form-add-post");
const txtEditTitle = document.querySelector("#txt-edit-title");
const txtEditBody = document.querySelector("#txt-edit-body");
const formEditPost = document.querySelector("#form-edit-post");
const divPost = document.querySelector("#div-post-entries");

let posts = [];

let postsInner;
let editId;

function showPosts() {
  postsInner = "";
  posts.forEach((post) => {
    if (post.isActive) {
      postsInner =
        postsInner +
        `<div class = "post">
      <h2>${post.title}</h2>
      <p>${post.body}</p>
      <p>postId: ${post.id}</p>
      <button onclick="editPost(${post.id})">Edit</button>
      <button onclick="deletePost(${post.id})">Delete</button>
      <button onclick="thumbsUp(${post.id}); return false;">👍 ${post.likes}</button>
      <button onclick="thumbsDown(${post.id}); return false;">👎 ${post.dislikes}</button>
      </div>  
      
      `;
    }
  });
  divPost.innerHTML = postsInner;
}

function thumbsUp(postId) {
  posts[postId].likes += 1;
  showPosts();

}
function thumbsDown(postId) {
  posts[postId].dislikes += 1;
  showPosts();
}

const initPage = async () => {
  response = await fetch("https://jsonplaceholder.typicode.com/posts");
  results = await response.json()
  posts = results;
  posts.forEach((post) => {
    post.id -= 1;
    post.isActive = true
    post.likes = 0

    post.dislikes = 0
  })
  console.log(posts)
  showPosts()
}
initPage();

// fetch("https://jsonplaceholder.typicode.com/posts").then((response) => response.json()).then((data) => {
//   posts = data
//   posts.forEach((post) => {
//     post.isActive = true
//   })
//   console.log(posts)
//   showPosts();
// });



formAddPost.addEventListener("submit", (event) => {
  event.preventDefault();
  posts.push({
    id: posts.length,
    title: txtTitle.value,
    body: txtBody.value,
    isActive: true,
  });
  txtTitle.value = "";
  txtBody.value = "";
  showPosts();
});

function editPost(postId) {
  txtEditTitle.value = posts[postId].title;
  txtEditBody.value = posts[postId].body;
  editId = postId;
}
function deletePost(postId) {
  posts[postId].isActive = false;
  showPosts();
}

formEditPost.addEventListener("submit", (event) => {
  event.preventDefault();

  posts[editId].title = txtEditTitle.value;
  posts[editId].body = txtEditBody.value;
  txtEditTitle.value = "";
  txtEditBody.value = "";
  editId = undefined;
  showPosts();
});


document.querySelector("#form-add-post").addEventListener(event) {
  event.preventDefault();
  fetch('https://jsonplaceholder.typicode.com/posts'), {
    method: 'POST',
    body: JSON.stringify({
      title: document.querySelector('#txt-title').value,
      body: document.querySelector('#txt-body').value,
      userId: 1,

    }),
    headers: { 'Content-type': 'application/json; charset=UTF-8' }
  }
}